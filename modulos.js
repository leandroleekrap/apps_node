// modulos sao formas de separar em varios blocos do seu codigo "blocos"

function mult(a, b){
    return a * b;

}

function testeHello(){
    var teste = true; // constante uma variavel que nao posso mudar o valor dela

        console.log("hello word");

        if(teste == false){
            console.log("o valor é falso");
        }else{
            console.log("o valor é true");
        }
}

console.log("a multiplicacao dos valores é", mult(10,5));
console.log(testeHello());