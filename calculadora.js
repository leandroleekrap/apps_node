// modulos sao formas de separar em varios blocos do seu codigo "blocos"
var nome = "Minha calculadora";

function soma(a, b){
    return a + b;
}

function sub(a, b){
    return a - b;
}

//module.exports = soma; // exportando a funcao, com isso conseguimos chama-la em qualquer parte do nosso programa

module.exports = {
    soma,
    sub,
    nome // importando uma variavel
}